%global _empty_manifest_terminate_build 0
Name:		python-opensensemap-api
Version:	0.3.2
Release:	1
Summary:	Python client for interacting with the openSenseMap API.
License:	MIT
URL:		https://github.com/home-assistant-ecosystem/python-opensensemap-api
Source0:	https://files.pythonhosted.org/packages/15/42/5c4ce62b8fe5ca04ebb905df5d60a698a6cd6897af07de1f39dc26efea71/opensensemap-api-0.3.2.tar.gz
BuildArch:	noarch

%description
Python Client for interacting with the openSenseMap API.

%package -n python3-opensensemap-api
Summary:	Python client for interacting with the openSenseMap API.
Provides:	python-opensensemap-api = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-aiohttp
Requires:	python3-async-timeout
%description -n python3-opensensemap-api
Python Client for interacting with the openSenseMap API.

%package help
Summary:	Development documents and examples for opensensemap-api
Provides:	python3-opensensemap-api-doc
%description help
Python Client for interacting with the openSenseMap API.

%prep
%autosetup -n opensensemap-api-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-opensensemap-api -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Oct 25 2024 zhangyulong <zhangyulong@kylins.cn> - 0.3.2-1
- Update package to version 0.3.2
  -Fix error messages with start example.py

* Tue May 16 2023 wubijie <wubijie@kylinos.cn> - 0.3.0-1
- Update package to version 0.3.0

* Thu Oct 20 2022 guozhengxin <guozhengxin@kylinos.cn> - 0.2.0-1
- Upgrade package to version 0.2.0

* Mon Jul 12 2021 Python_Bot <Python_Bot@openeuler.org> - 0.1.6-1
- Package Spec generated
